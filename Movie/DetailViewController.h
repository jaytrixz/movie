//
//  DetailViewController.h
//  Movie
//
//  Created by Jay Santos on 5/23/15.
//  Copyright (c) 2015 Appsaint. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Movie.h"

@interface DetailViewController : UIViewController

@property (strong, nonatomic) Movie *detailViewControllerMovie;

@property (weak, nonatomic) IBOutlet UIImageView *backdropImage;
@property (weak, nonatomic) IBOutlet UIImageView *coverImage;
@property (weak, nonatomic) IBOutlet UILabel *movieTitle;
@property (weak, nonatomic) IBOutlet UILabel *year;
@property (weak, nonatomic) IBOutlet UILabel *rating;
@property (weak, nonatomic) IBOutlet UITextView *overview;
@end

