//
//  MasterTableViewCell.h
//  Movie
//
//  Created by Jay Santos on 5/24/15.
//  Copyright (c) 2015 Appsaint. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *year;
@property (strong, nonatomic) IBOutlet UILabel *title;

@property (strong, nonatomic) IBOutlet UIImageView *image;

@end
