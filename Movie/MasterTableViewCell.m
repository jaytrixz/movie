//
//  MasterTableViewCell.m
//  Movie
//
//  Created by Jay Santos on 5/24/15.
//  Copyright (c) 2015 Appsaint. All rights reserved.
//

#import "MasterTableViewCell.h"

@implementation MasterTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
