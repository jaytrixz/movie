//
//  NetworkManager.h
//  Movie
//
//  Created by Jay Santos on 5/24/15.
//  Copyright (c) 2015 Appsaint. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkManager : NSObject

+ (instancetype)sharedManager;

- (void)getJSON:(void (^)(id jsonResponse))success failure:(void (^)(NSError *error))failure;
- (void)loadImageWithImage:(id)movieImage url:(NSString *)url success:(void (^)(id image))success failure:(void (^)(NSError *error))failure;

@end
