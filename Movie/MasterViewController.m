//
//  MasterViewController.m
//  Movie
//
//  Created by Jay Santos on 5/23/15.
//  Copyright (c) 2015 Appsaint. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"

#import "Movie.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "NetworkManager.h"
#import "MasterTableViewCell.h"
#import "UIImageView+AFNetworking.h"

@interface MasterViewController ()

@property (strong, nonatomic) Movie *movie;

@property (strong, nonatomic) NSMutableArray *moviesList;

@end

@implementation MasterViewController

#pragma mark - Lifecycle

- (void)awakeFromNib {
    [super awakeFromNib];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.moviesList = [NSMutableArray array];
    
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    [self loadJSON];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailViewControllerMovie:self.moviesList[indexPath.row]];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.moviesList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MasterTableViewCell *cell = (MasterTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    [self configureMasterTableCellWithTableCell:cell indexPath:indexPath];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kTableCellHeight;
}

- (void)configureMasterTableCellWithTableCell:(MasterTableViewCell *)cell indexPath:(NSIndexPath *)indexPath {
    Movie *movie = self.moviesList[indexPath.row];
    cell.title.text = movie.title;
    cell.year.text = [movie.year stringValue];
    
    [[NetworkManager sharedManager] loadImageWithImage:cell.image url:[NSString stringWithFormat:@"%@%@%@", kImageURL, movie.slug, kBackdropImage] success:^(UIImage *image) {
        cell.image.image = image;
    } failure:^(NSError *error) {
        NSLog(@"Error loading image at index: %ld", (long)indexPath.row);
    }];
}

#pragma mark - JSON structure

- (void)loadJSON {
    [[NetworkManager sharedManager] getJSON:^(id jsonResponse) {
        [self loadMovieWithJSON:jsonResponse];
    } failure:^(NSError *error) {
        [[[UIAlertView alloc] initWithTitle:kErrorTitle message:[error localizedDescription] delegate:self cancelButtonTitle:nil otherButtonTitles:kErrorButtonTitle, nil] show];
    }];
}

#pragma mark - Movie structure

- (void)loadMovieWithJSON:(id)json {
    NSArray *movies = json[@"data"][@"movies"];
    
    if ([self.moviesList count] != 0) {
        [self.moviesList removeAllObjects];
    }
    
    for (NSDictionary *movie in movies) {
        self.movie = [[Movie alloc] initWithRating:movie[@"rating"] genre:movie[@"genres"] language:movie[@"language"] title:movie[@"title"] url:movie[@"url"] longTitle:movie[@"title_long"] imdbCode:movie[@"imdb_code"] identifier:movie[@"id"] state:movie[@"state"] year:movie[@"year"] runtime:movie[@"runtime"] overview:movie[@"overview"] slug:movie[@"slug"] mpaRating:movie[@"mpa_rating"]];
        
        [self.moviesList addObject:self.movie];
    }
    
    [self.tableView reloadData];
}

@end
