//
//  Constants.h
//  Movie
//
//  Created by Jay Santos on 5/24/15.
//  Copyright (c) 2015 Appsaint. All rights reserved.
//

#ifndef Movie_Constants_h
#define Movie_Constants_h

// Links or URLs
#define kMainURL                    @"https://dl.dropboxusercontent.com/u/5624850/movielist/"
#define kJsonURL                    kMainURL "list_movies_page1.json"
#define kImageURL                   kMainURL "images/"

// Images
#define kCoverImage                 @"-cover.jpg"
#define kBackdropImage              @"-backdrop.jpg"

// Cells
#define kTableCellHeight            200

// Titles
#define kErrorTitle                 @"Sorry"
#define kErrorButtonTitle           @"OK"

#endif
