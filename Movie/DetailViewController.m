//
//  DetailViewController.m
//  Movie
//
//  Created by Jay Santos on 5/23/15.
//  Copyright (c) 2015 Appsaint. All rights reserved.
//

#import "DetailViewController.h"

#import "Constants.h"
#import "NetworkManager.h"
#import "UIImageView+AFNetworking.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailViewControllerMovie:(Movie *)detailViewControllerMovie {
    if (_detailViewControllerMovie != detailViewControllerMovie) {
        _detailViewControllerMovie = detailViewControllerMovie;
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailViewControllerMovie) {
        self.movieTitle.text = self.detailViewControllerMovie.title;
        self.year.text = [self.detailViewControllerMovie.year stringValue];
        self.rating.text = [NSString stringWithFormat:@"%.2f", [self.detailViewControllerMovie.rating floatValue]];
        self.overview.text = self.detailViewControllerMovie.overview;
        
        [[NetworkManager sharedManager] loadImageWithImage:self.backdropImage url:[NSString stringWithFormat:@"%@%@%@", kImageURL, self.detailViewControllerMovie.slug, kBackdropImage] success:^(id image) {
            self.backdropImage.image = image;
        } failure:^(NSError *error) {
            NSLog(@"Failed to load backdrop image in movie details with error: %@", error);
        }];
        
        [[NetworkManager sharedManager] loadImageWithImage:self.coverImage url:[NSString stringWithFormat:@"%@%@%@", kImageURL, self.detailViewControllerMovie.slug, kCoverImage] success:^(id image) {
            self.coverImage.image = image;
        } failure:^(NSError *error) {
            NSLog(@"Failed to load cover image in movie details with error: %@", error);
        }];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
