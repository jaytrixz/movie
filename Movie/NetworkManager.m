//
//  NetworkManager.m
//  Movie
//
//  Created by Jay Santos on 5/24/15.
//  Copyright (c) 2015 Appsaint. All rights reserved.
//

#import "NetworkManager.h"

#import "Constants.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"

@implementation NetworkManager

+ (instancetype)sharedManager {
    __strong static NetworkManager *sharedManager = nil;
    static dispatch_once_t token = 0;
    dispatch_once(&token, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

- (void)getJSON:(void (^)(id jsonResponse))success failure:(void (^)(NSError *error))failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
    [manager GET:kJsonURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error getting JSON with error: %@", error);
        failure(error);
    }];
}

- (void)loadImageWithImage:(id)movieImage url:(NSString *)url success:(void (^)(id image))success failure:(void (^)(NSError *error))failure {
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:60];
    
    [movieImage setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageNamed:@"placeholder"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        success(image);
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        NSLog(@"Error loading image with error: %@", error);
        failure(error);
    }];
}

@end
