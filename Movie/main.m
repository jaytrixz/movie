//
//  main.m
//  Movie
//
//  Created by Jay Santos on 5/23/15.
//  Copyright (c) 2015 Appsaint. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
