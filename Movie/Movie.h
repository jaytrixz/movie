//
//  Movie.h
//  Movie
//
//  Created by Jay Santos on 5/24/15.
//  Copyright (c) 2015 Appsaint. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Movie : NSObject

@property (readonly, nonatomic) NSArray *genre;

@property (readonly, nonatomic) NSString *url;
@property (readonly, nonatomic) NSString *slug;
@property (readonly, nonatomic) NSString *title;
@property (readonly, nonatomic) NSString *state;
@property (readonly, nonatomic) NSString *overview;
@property (readonly, nonatomic) NSString *language;
@property (readonly, nonatomic) NSString *imdbCode;
@property (readonly, nonatomic) NSString *longTitle;
@property (readonly, nonatomic) NSString *mpaRating;
@property (readonly, nonatomic) NSString *identifier;

@property (readonly, nonatomic) NSNumber *year;
@property (readonly, nonatomic) NSNumber *rating;
@property (readonly, nonatomic) NSNumber *runtime;

- (instancetype)initWithRating:(NSNumber *)rating genre:(NSArray *)genre language:(NSString *)language title:(NSString *)title url:(NSString *)url longTitle:(NSString *)longTitle imdbCode:(NSString *)imdbCode identifier:(NSString *)identifier state:(NSString *)state year:(NSNumber *)year runtime:(NSNumber *)runtime overview:(NSString *)overview slug:(NSString *)slug mpaRating:(NSString *)mpaRating;

@end
