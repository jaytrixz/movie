//
//  Movie.m
//  Movie
//
//  Created by Jay Santos on 5/24/15.
//  Copyright (c) 2015 Appsaint. All rights reserved.
//

#import "Movie.h"

@implementation Movie

- (instancetype)initWithRating:(NSNumber *)rating genre:(NSArray *)genre language:(NSString *)language title:(NSString *)title url:(NSString *)url longTitle:(NSString *)longTitle imdbCode:(NSString *)imdbCode identifier:(NSString *)identifier state:(NSString *)state year:(NSNumber *)year runtime:(NSNumber *)runtime overview:(NSString *)overview slug:(NSString *)slug mpaRating:(NSString *)mpaRating {
    if (self == [super init]) {
        _rating = rating;
        _genre = genre;
        _language = language;
        _title = title;
        _url = url;
        _longTitle = longTitle;
        _imdbCode = imdbCode;
        _identifier = identifier;
        _state = state;
        _year = year;
        _runtime = runtime;
        _overview = overview;
        _slug = slug;
        _mpaRating = mpaRating;
    }
    
    return self;
}

@end
