//
//  AppDelegate.h
//  Movie
//
//  Created by Jay Santos on 5/23/15.
//  Copyright (c) 2015 Appsaint. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

